package longestPlateau;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class LargestPlateauTest {
    private int[] givenArray;

    private int[] expectedResult;

    private Plateau plateau;

    public void Plateau(int[] givenArray, int[] expectedResult) {
        this.givenArray = givenArray;
        this.expectedResult = expectedResult;
    }

    @Before

    @Parameterized.Parameters
    public static int[][][] data() {
        return new int[][][]{
                {{1, 2, 3, 3, 3, 1}, {1, 2}},
                {{1, 2, 3, 3, 3, 4, 5, 5, 4}, {5, 3}},
                {{1, 2, 3, 3, 3, 2, 5, 5, 4}, {5, 4}},
                {{-1, 1, 1, 1, 0}, {1, 3}}
        };
    }

    @Test
    @DisplayName("Find the longest plateau")
    public void test_findLongestPlateauIn() {
//        assertThat(plateau.findLongestPlateauIn(givenArray), is(expectedResult));
    }
}
